"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.addColumn("Transactions", "status", {
      type: Sequelize.STRING,
    });
    await queryInterface.addColumn("Transactions", "order_id", {
      type: Sequelize.STRING,
    });
    await queryInterface.addColumn("Transactions", "external_tx_id", {
      type: Sequelize.STRING,
    });
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.removeColumn("Transactions", "status", null);
    await queryInterface.removeColumn("Transactions", "order_id", null);
    await queryInterface.removeColumn("Transactions", "external_tx_id", null);
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  },
};
